package de.mewin.balloons;

import java.util.Collection;
import java.util.HashMap;

public class BalloonRegistry
{
    private final HashMap<String, BalloonType> balloonTypes = new HashMap<>();

    public void registerBalloonType(String name, BalloonType balloonType)
    {
        balloonTypes.put(name.toLowerCase(), balloonType);
    }

    public BalloonType getBalloonType(String name)
    {
        return balloonTypes.get(name.toLowerCase());
    }

    public Collection<String> getBalloonTypes()
    {
        return balloonTypes.keySet();
    }
}
