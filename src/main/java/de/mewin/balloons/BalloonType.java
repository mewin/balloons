package de.mewin.balloons;

import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

import java.util.List;

public abstract class BalloonType
{
    protected static final Vector POS_OFFSET = new Vector(0.0, 2.0, 0.0);

    public abstract LivingEntity createBalloon(Player creator, List<String> args) throws InvalidParamsException;

    public List<String> completeArgs(String[] args)
    {
        return null;
    }

    public static final class InvalidParamsException extends Exception
    {
        public InvalidParamsException(String message)
        {
            super(message);
        }
    }
}
