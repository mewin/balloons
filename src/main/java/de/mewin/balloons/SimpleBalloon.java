package de.mewin.balloons;

import org.bukkit.World;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;

import java.util.List;

public class SimpleBalloon extends BalloonType
{
    private final EntityType entityType;

    public SimpleBalloon(EntityType entityType)
    {
        if (!entityType.isAlive() || !entityType.isSpawnable()) {
            throw new IllegalArgumentException("invalid entity type");
        }
        this.entityType = entityType;
    }

    @Override
    public LivingEntity createBalloon(Player creator, List<String> args)
    {
        World world = creator.getWorld();
        return (LivingEntity) world.spawnEntity(creator.getLocation().add(POS_OFFSET), entityType);
    }
}
