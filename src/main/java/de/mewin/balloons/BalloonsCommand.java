package de.mewin.balloons;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class BalloonsCommand implements CommandExecutor, TabCompleter
{
    private final BalloonsPlugin plugin;

    public BalloonsCommand(BalloonsPlugin plugin)
    {
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args)
    {
        if (!(sender instanceof Player))
        {
            sender.sendMessage(ChatColor.RED + "sorry, only players get balloons :(");
            return true;
        }

        if (args.length < 1) {
            return false;
        }

        BalloonRegistry balloonRegistry = plugin.getBalloonRegistry();
        BalloonType balloonType = balloonRegistry.getBalloonType(args[0]);

        if (balloonType == null)
        {
            ArrayList<String> types = new ArrayList<>(balloonRegistry.getBalloonTypes());
            types.sort(String::compareTo);

            sender.sendMessage(ChatColor.RED + "Unknown balloon type, valid values are:");
            sender.sendMessage(ChatColor.RED + String.join(", ", types));
        }


        plugin.createBalloon((Player) sender, balloonType, Arrays.stream(args).skip(1).collect(Collectors.toList()));

        return true;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, String alias, String[] args)
    {
        if (args.length == 1)
        {
            String name = args[0].toLowerCase();

            BalloonRegistry balloonRegistry = plugin.getBalloonRegistry();

            return balloonRegistry.getBalloonTypes()
                    .stream()
                    .filter(str -> str.startsWith(name))
                    .collect(Collectors.toList());
        }
        else if (args.length > 1)
        {
            String name = args[0].toLowerCase();
            BalloonRegistry balloonRegistry = plugin.getBalloonRegistry();
            BalloonType balloonType = balloonRegistry.getBalloonType(name);

            if (balloonType == null) {
                return null;
            }

            return balloonType.completeArgs(args);
        }

        return null;
    }
}
