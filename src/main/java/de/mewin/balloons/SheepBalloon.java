package de.mewin.balloons;

import org.bukkit.DyeColor;
import org.bukkit.World;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Sheep;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class SheepBalloon extends BalloonType
{
    @Override
    public LivingEntity createBalloon(Player creator, List<String> args) throws InvalidParamsException
    {
        World world = creator.getWorld();
        Sheep sheep = (Sheep) world.spawnEntity(creator.getLocation().add(POS_OFFSET), EntityType.SHEEP);

        if (args.size() > 0)
        {
            try
            {
                DyeColor color = DyeColor.valueOf(args.get(0).toUpperCase());
                sheep.setColor(color);
            }
            catch(IllegalArgumentException ex) {
                throw new InvalidParamsException("invalid sheep color: " + args.get(0));
            }
        }

        return sheep;
    }

    @Override
    public List<String> completeArgs(String[] args)
    {
        if (args.length < 2) {
            return null;
        }

        String color = args[1].toLowerCase();

        return Arrays.stream(DyeColor.values())
                .map(dyeColor -> dyeColor.name())
                .map(String::toLowerCase)
                .filter(str -> str.startsWith(color))
                .collect(Collectors.toList());
    }
}
