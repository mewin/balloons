package de.mewin.balloons;

import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.List;

/**
 *
 * @author mewin
 */
public class BalloonsPlugin extends JavaPlugin
{
    public static final String BALLOON_TYPE_COW = "cow";
    public static final String BALLOON_TYPE_PIG = "pig";
    public static final String BALLOON_TYPE_CHICKEN = "chicken";
    public static final String BALLOON_TYPE_SHEEP = "sheep";
    public static final String COMMAND_BALLOONS = "balloons";
    public static final int TICKS_FOREVER = 20000000;

    private final BalloonRegistry balloonRegistry = new BalloonRegistry();

    @Override
    public void onEnable()
    {
        BalloonsCommand balloonsCommand = new BalloonsCommand(this);
        getCommand(COMMAND_BALLOONS).setExecutor(balloonsCommand);
        getCommand(COMMAND_BALLOONS).setTabCompleter(balloonsCommand);
        registerDefaultBalloonTypes();
    }

    private void registerDefaultBalloonTypes()
    {
        balloonRegistry.registerBalloonType(BALLOON_TYPE_COW, new SimpleBalloon(EntityType.COW));
        balloonRegistry.registerBalloonType(BALLOON_TYPE_PIG, new SimpleBalloon(EntityType.PIG));
        balloonRegistry.registerBalloonType(BALLOON_TYPE_CHICKEN, new SimpleBalloon(EntityType.CHICKEN));
        balloonRegistry.registerBalloonType(BALLOON_TYPE_SHEEP, new SheepBalloon());
    }

    public void createBalloon(Player creator, BalloonType type, List<String> args)
    {
        try
        {
            LivingEntity entity = type.createBalloon(creator, args);
            entity.addPotionEffect(new PotionEffect(PotionEffectType.LEVITATION, TICKS_FOREVER, 0, false, false));
            entity.setLeashHolder(creator);
        }
        catch(BalloonType.InvalidParamsException ex)
        {
            creator.sendMessage(ex.getMessage());
        }
    }

    public BalloonRegistry getBalloonRegistry()
    {
        return balloonRegistry;
    }

    public static BalloonsPlugin get()
    {
        return getPlugin(BalloonsPlugin.class);
    }
}
