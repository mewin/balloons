# Balloons

Bukkit/Spigot plugin that allows players to create balloon animals.

## Getting Started

No dependencies except Bukkit and Java 8, simply drop the jar into your plugin folder. The plugin was created for Bukkit 1.13.2. It may or may not work with other versions.

## Built With

* [Maven](https://maven.apache.org/) - Dependency Management

## Contributing

Just create a pull request :).

## Authors

* **Patrick Wuttke** - *Initial work* - [mewin](https://gitlab.com/mewin)

## License

This project is licensed under the MIT License - see the [LICENSE.txt](LICENSE.txt) file for details

## Acknowledgments

* The team of [Avalunia](https://avalunia.de) ;)

